*&---------------------------------------------------------------------*
*& Report z_test_tag_api
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_tag_api_2.

PARAMETERS: repo_key TYPE zif_abapgit_persistence=>ty_value OBLIGATORY,
            user     TYPE string OBLIGATORY LOWER CASE,
            password TYPE string OBLIGATORY LOWER CASE,
            tag      TYPE string OBLIGATORY LOWER CASE.

CLASS controller DEFINITION.

  PUBLIC SECTION.
    METHODS: start.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD start.

    DATA: lo_repo TYPE REF TO zcl_abapgit_repo_online.

    TRY.
        lo_repo ?= zcl_abapgit_repo_srv=>get_instance( )->get( repo_key ).

        zcl_abapgit_login_manager=>set( iv_uri      = lo_repo->get_url( )
                                        iv_username = user
                                        iv_password = password ).

        zcl_abapgit_git_porcelain=>create_tag( io_repo = lo_repo
                                               is_tag  = value #( name = zcl_abapgit_tag=>add_tag_prefix( tag ) ) ).

*        DATA(lo_abapgit_facade) = NEW zcl_abapgit_facade( iv_key      = repo_key
*                                                          iv_user     = user
*                                                          iv_password = password  ).
*
*        DATA(message) = lo_abapgit_facade->create_tag( iv_tag_name = tag ).


      CATCH zcx_abapgit_exception INTO DATA(error).
        MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
    ENDTRY.

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW controller( )->start( ).
