*&---------------------------------------------------------------------*
*& Report z_test_commit_api
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_commit_api.

PARAMETERS: repo_key TYPE zif_abapgit_persistence=>ty_value OBLIGATORY,
            user     TYPE string OBLIGATORY LOWER CASE,
            password TYPE string OBLIGATORY LOWER CASE,
            comment  TYPE string OBLIGATORY LOWER CASE.


TRY.
    DATA(lo_abapgit_facade) = NEW zcl_abapgit_facade( iv_key      = repo_key
                                                      iv_user     = user
                                                      iv_password = password ).

    lo_abapgit_facade->create_commit(
      EXPORTING
        iv_committer_name    = 'Christian Guenter'
        iv_committer_email   = 'christianguenter@googlemail.com'
        iv_comment           = comment
        iv_remove            = abap_true
      IMPORTING
        ev_nothing_to_commit = DATA(lv_nothing_to_commit)
        et_staged            = DATA(lt_staged)
        et_removed           = DATA(lt_removed) ).

  CATCH zcx_abapgit_exception INTO DATA(error).
    MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
    RETURN.
ENDTRY.

IF lv_nothing_to_commit = abap_true.
  MESSAGE 'Nothing to commit!' TYPE 'I'.
  RETURN.
ENDIF.

cl_demo_output=>write( lt_staged ).
cl_demo_output=>write( lt_removed ).
cl_demo_output=>display(  ).
