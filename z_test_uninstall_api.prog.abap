*&---------------------------------------------------------------------*
*& Report z_test_clone_api
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_uninstall_api.

PARAMETERS: repo_key TYPE zif_abapgit_persistence=>ty_value OBLIGATORY.

TRY.
    DATA(lo_abapgit_facade) = NEW zcl_abapgit_facade( iv_key =  repo_key ).

    lo_abapgit_facade->uninstall(  ).

  CATCH zcx_abapgit_exception INTO DATA(error).
    MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
    RETURN.
  CATCH zcx_abapgit_cancel INTO DATA(cancel).
    MESSAGE cancel TYPE 'S' DISPLAY LIKE 'E'.
    RETURN.
ENDTRY.
