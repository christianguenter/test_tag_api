*&---------------------------------------------------------------------*
*& Report z_test_injector
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_injector.

*
*CLASS zcl_abapgit_message_collector DEFINITION
*  PUBLIC
*  FINAL
*  CREATE PUBLIC .
*
*
*  PUBLIC SECTION.
*    INTERFACES: zif_abapgit_message.
*
*    METHODS:
*      get_last_message
*        RETURNING
*          VALUE(rv_last_message) TYPE string.
*
*  PRIVATE SECTION.
*    DATA:
*      mt_message TYPE stringtab.
*
*ENDCLASS.
*
*
*
*CLASS zcl_abapgit_message_collector IMPLEMENTATION.
*
*  METHOD get_last_message.
*
*    IF lines( mt_message ) = 0.
*      RETURN.
*    ENDIF.
*
*    READ TABLE mt_message INDEX lines( mt_message )
*                          INTO rv_last_message.
*
*  ENDMETHOD.
*
*  METHOD zif_abapgit_message~success.
*
*    INSERT iv_text INTO TABLE mt_message.
*
*  ENDMETHOD.
*
*ENDCLASS.

*TRY.
*    DATA(lo_message_collector) = NEW zcl_abapgit_message_collector( ).
*    DATA(lo_popup_provider) = NEW zcl_abapgit_popup_provider( ).
*
*    zcl_abapgit_di_container=>set_instance( iv_name   = |ZIF_ABAPGIT_POPUPS|
*                                            io_object = lo_message_collector ).
*
*  CATCH zcx_abapgit_exception INTO DATA(error).
*    MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
*ENDTRY.
