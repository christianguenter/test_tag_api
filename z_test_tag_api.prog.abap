*&---------------------------------------------------------------------*
*& Report z_test_tag_api
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_tag_api.

PARAMETERS: repo_key TYPE  zif_abapgit_persistence=>ty_value OBLIGATORY,
            user     TYPE string OBLIGATORY LOWER CASE,
            password TYPE string OBLIGATORY LOWER CASE,
            tag      TYPE string OBLIGATORY LOWER CASE.

TRY.
    DATA(lo_abapgit_facade) = NEW zcl_abapgit_facade( iv_key      = repo_key
                                                      iv_user     = user
                                                      iv_password = password  ).

    DATA(message) = lo_abapgit_facade->create_tag( iv_tag_name = tag ).

    MESSAGE message TYPE 'I'.

  CATCH zcx_abapgit_exception INTO DATA(error).
    MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
ENDTRY.
