*&---------------------------------------------------------------------*
*& Report z_test_clone_api
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_clone_api.

PARAMETERS: url      TYPE string OBLIGATORY LOWER CASE,
            package  TYPE string OBLIGATORY LOWER CASE,
            user     TYPE string OBLIGATORY LOWER CASE,
            password TYPE string OBLIGATORY LOWER CASE.

TRY.
    DATA(lo_abapgit_facade) = NEW zcl_abapgit_facade( iv_key      = ''
                                                      iv_user     = user
                                                      iv_password = password ).

    lo_abapgit_facade->clone(
        iv_url     = url
        iv_package = CONV #( package ) ).
  CATCH zcx_abapgit_exception INTO DATA(error).
    MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
    RETURN.
  CATCH zcx_abapgit_cancel INTO DATA(cancel).
    MESSAGE cancel TYPE 'S' DISPLAY LIKE 'E'.
    RETURN.
ENDTRY.
